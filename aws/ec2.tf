// ---- 5. Instances (Virtual Machines) ----
//Mount the Instance (VM) #1
resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id //Linked image to deploy VM. //*Data
  instance_type = "t2.micro"
  key_name      = aws_key_pair.deployer.key_name //*Resource. I want to associate the ssh key to MV to be able to login
  //Rules of firewall that i want to open or close
  vpc_security_group_ids = [
    aws_security_group.allow_http_ssh.id
  ]

  user_data = "${file("install_apache_server1.sh")}"
  subnet_id = local.subnet_a

  tags = {
    Name = "UbServer1"
  }
}

//Mount the Instance (VM) #2
resource "aws_instance" "web2" {
  ami           = data.aws_ami.ubuntu.id //Linked image to deploy VM. //*Data
  instance_type = "t2.micro"
  key_name      = aws_key_pair.deployer.key_name //*Resource. I want to associate the ssh key to MV to be able to login
  //Rules of firewall that i want to open or close
  vpc_security_group_ids = [
    aws_security_group.allow_http_ssh.id
  ]

  user_data = "${file("install_apache_server2.sh")}"
  subnet_id = local.subnet_b

  tags = {
    Name = "UbServer2"
  }
}