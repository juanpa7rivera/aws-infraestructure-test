// ---- Network LoadBalancer -----
//Subnet to assign to each instance (Availability zones)
locals {
  subnet_a = "subnet-05bb240e0798dc373"
  subnet_b = "subnet-0799810618c08159e"
}

//Resource LoadBalancer
resource "aws_lb" "load_balancer" {
  name = "nlb-apache"
  load_balancer_type = "network"
  subnets = [local.subnet_a, local.subnet_b]
  enable_deletion_protection = false
  
  //Distribute network traffic among registered destinations in each Availability Zone
  enable_cross_zone_load_balancing = true

  tags = {
    Name = "nlb-apache",
    Entity = "Juan Pablo Rivera",
    Creator = "Terraform"
  }
  depends_on = [aws_instance.web, aws_instance.web2]
}

//What the LoadBalancer is going to look for to confirm that the service (Instance) is alive
//LoadBalancer Target Group
resource "aws_lb_target_group" "lb_target" {
  name = "tg-apache"
  port = 80
  protocol = "TCP"
  target_type = "instance"
  vpc_id = var.vpc_id
  depends_on = [aws_lb.load_balancer]

  health_check {
    protocol = "HTTP"
  }

}

//Listener PORT 80: Listener that will use LB to publish service to the outside and pass the traffic to the target group
resource "aws_lb_listener" "front_end_80" {
  load_balancer_arn = aws_lb.load_balancer.arn
  port = 80
  protocol = "TCP"
  default_action {
    type = "forward" //Redirection traffic of Target Group
    target_group_arn = aws_lb_target_group.lb_target.arn
  }
  depends_on = [aws_lb_target_group.lb_target]
}

//Join instance 1 to target group
resource "aws_lb_target_group_attachment" "instance1_attachment_lb" {
  target_group_arn = aws_lb_target_group.lb_target.arn
  target_id = aws_instance.web.id
  port = 80
  depends_on = [aws_instance.web, aws_lb_target_group.lb_target]
}

//Join instance 2 to target group
resource "aws_lb_target_group_attachment" "instance2_attachment_lb" {
  target_group_arn = aws_lb_target_group.lb_target.arn
  target_id = aws_instance.web2.id
  port = 80
  depends_on = [aws_instance.web2, aws_lb_target_group.lb_target]
}