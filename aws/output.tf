//---- 6. Bash outputs ----
//GET the public ipv4 of VM
output "ip_instance" {
  value = aws_instance.web.public_ip
}
output "ip_instance2" {
  value = aws_instance.web2.public_ip
}
//ssh to deploy VM
output "ssh" {
  value = "ssh -l ubuntu ${aws_instance.web.public_ip}"
}
//ssh to deploy VM #2
output "ssh2" {
  value = "ssh -l ubuntu ${aws_instance.web2.public_ip}"
}
//address http VM #1
output "url1" {
  value = "http://${aws_instance.web.public_ip}/"
}
//address http VM #1
output "url2" {
  value = "http://${aws_instance.web2.public_ip}/"
}

// Network Load Balance
output "http_80_NLB" {
  value = "http://${aws_lb.load_balancer.dns_name}"
  description = "HTTP Link port 80"
}