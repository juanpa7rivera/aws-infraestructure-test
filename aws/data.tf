//---- 2. Data file
//Find last ubuntu image to deploy Instances
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-20220420"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  //Owner of local image
  owners = ["099720109477"] # Canonical
}