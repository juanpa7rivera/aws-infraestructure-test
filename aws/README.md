# Test AWS Apprenticeship | Developed by Juan Pablo Rivera Jiménez

####  1. I install the latest version of Ubuntu on my computer (Ubuntu 22.04)
![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/0.9.png)
####  2. I create a root AWS account
![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/1.png)
 ######  2.1 I secure the account
 - I’m going to activate the MFA (Multifactor authentication) for improve security
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/2.png)
 - I create a group called admins
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/3.png)
 - Then I will create a IAM user, this will have admin permissions. It will be in the previously created admins group
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/4.png)
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/5.png)
 - I activate the MFA again, but in this case for juanpablo user
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/6.png)
 - I generate the Access key (credential) for this user
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/7.png)
#### 3.  I install Terraform: It will allow defining the configurations and definitions of a service infrastructure in the cloud.
![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/8.png)
- I verify the installation
![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/9.png)
#### 4. I use AWS CLI to deploy my servers
![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/10.png)
 - Then, I verify my AWS version
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/11.png)
#### 5.  I create the credentials for start working with AWS servers
![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/12.png)
![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/12.2.png)
 - Also I configure the AWS CLI. Result of basic configuration of AWS cli
![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/13.png)
![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/13.1.png)
#### 6.  I create an Infraestructure directory. This will allow me to create 2 instances in AWS. I use the code editor Visual Studio Code to open the directory that I have created
![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/13.2.png)
 ###### 6.1  Next, I create a GitLab account to save the changes that I have, and I create new repo ("aws-infraestructure-test") and a new branch ("juanpablo")
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/13.3.png)
####  7. I create a ssh key to access the aws console through linux bash
![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/14.png)
 - I verify the successful creation of the keys (id_rsa and id_rsa.pub)
![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/15.png)
####  8. I will create two instances (virtual machines) in AWS through Terraform
 ###### 8.1 I create a main file that will have the cloud infrastructure provider, that contains the variable that indicates the ssh key path, the variable that indicates the id of the VPC (virtual private cloud) and the resource to assign the ssh key.
 ###### VPC is a virtual network that will allow me to launch AWS resources/services
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/16.png)
 ###### 8.2 I create a file that will have the data of the Ubuntu image of the server
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/16.1.png)
 ###### 8.3 Now I create a file terraform.tfvars that will allow me to put values to the variables that I defined in [main.tf](https://gitlab.com/juanpa7rivera/aws-infraestructure-test/-/blob/juanpablo/aws/main.tf)
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/16.1.1.png)
 ###### 8.4 I create a security group, these will allow me to indicate what services/ports I will need in my network
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/16.2.png)
 ###### 8.5 Then, in a file that I called ec2, there will be the information of each of the instances (virtual machines), the type, the ssh key, the security groups and the call to the script that installed the apache web server
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/16.3.png)

 Script to install apache web server in each Instance
 
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/16.4.png)
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/16.4.2.png)
 ###### 8.6 I create a file that prints information about the servers in bash
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/16.5.png)
####  9. Initialize the Terraform working directory, this will download the necessary plugins for deployment, with command: "terraform init"
![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/16.6.png)
 ###### 9.1 I plan the deployment, this checks for errors, with command: "terraform plan"
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/16.7.png)
 ###### 9.2 Execute what is proposed in the plan  (deploy the instances)
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/16.8.png)
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/16.9.png)
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/16.9.1.png)
 
####  10. I  verify my Instances in AWS
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/17.png)
 - Instance #1 Information
   ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/17.01.png)
 - Instance #2 Information
   ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/17.02.png)
 
 ###### 10.1 I connect to each instance via ssh and verify the status of each apache server

- Instance (VM) #1

 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/17.1.png)
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/17.2.png)

- Instance (VM) #2

 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/17.3.png)
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/17.4.png)

 ###### 10.3 I access to the server of each Instace 
 - URL Virtual Machine (VM) #1

  http://3.85.168.168/

  ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/17.5.png)

 - URL Virtual Machine (VM) #2

  http://34.201.94.97/

  ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/17.6.png)

#### 11. I start with Network Load Balancer (NLB)
- Network Load Balancer redirects incoming traffic (access request from a client to a server) to various targets/destinations using the TCP protocol, helping to balance the network load.
 ######  11.1  I create a [lb.tf](https://gitlab.com/juanpa7rivera/aws-infraestructure-test/-/blob/juanpablo/aws/lb.tf) file, that contains the configuration of NLB (Network Load Balancer)
 ###### -  Having the vpc, I must assign each instance an available ip address of the vpc. This is called a subnet. The idea is to have an instance in each availability zone
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/18.png)
  ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/18.1.png)
 ###### 11.2 I create aws_lb that provides Load Balancer resource
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/18.2.png)
 ###### 11.3 I define the LoadBalancer Target Group. It is what the LoadBalancer is going to look for to confirm that the service (Instance) is alive
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/18.3.png)
 ###### 11.4 I define the listener. It will use LoadBalancer to publish service to outside.
  ###### - This would be a kind of redirection from the external side of the LB to the internal port 80 of each instance
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/18.4.png)
 ###### 11.5 Join instances to Target Group that I have defined previously
  ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/18.5.png)
 ###### 11.6 I define a new security group for LoadBalancer
 ###### - To allow access from the Load Balancer service
  ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/18.6.png)
 ###### 11.7 I modify my existing security group, indicating that the origin will be the sg of the LoadBalancer and not any origin
  ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/18.7.png)
 ###### 11.8 I add dns_name that is generated from the Load Blancer to the ouput file
   ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/18.8.png)
 ###### 11.9  I execute terraform init, terraform plan and terraform apply to save the changes. Then, this is the output result:
   ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/19.png)

###### 12. I access the NLB (Network Load Balancer) through port 80
 Network load Balancer redirects incoming traffic to various targets/destinations using the TCP protocol, helping to balance the network load.
 In this case, we can see how from the NLB you can redirect to VM1 or VM2.
 - URL Network Load Balance (via Port 80)

 http://nlb-apache-dcaef991f75e8f84.elb.us-east-1.amazonaws.com

   ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/20.png)
   ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/20.1.png)
   
 Finally:
- I verify my Network Load Balancer in AWS
  ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/20.2.png)
  - I check Target Group in AWS
   ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/20.2.1.png)
  - NLB Listeners (Port 80) in AWS
    ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/20.3.png)
 - I verify my Security Groups in AWS
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/20.4.png)
- Also, I verify my Key pairs in AWS
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/20.5.png)
 - I view my complete EC2 Dashboard in AWS
 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/20.6.png)
 - The infrastructure diagram looks like this

 ![](https://www.distrigasdelsur.com/test_aws_apprenticeship_softserve_jprj/diagram.png)
 
Everything looks fine ✨🚀