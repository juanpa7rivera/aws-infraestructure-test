//----- 1. Main File -----
//Provider AWS
provider "aws" {
  region = "us-east-1"
}

//I define the route key ssh and id of vpc (virtual private cloud)
variable "ssh_key_path" {}
variable "vpc_id" {}

//Resources in Terraform
/*
AWS needs to have that public key, so that when it creates the VM, that key is associated with each VM. When we log in, we will be among the authorized keys.
*/
//Public key: Is associated with each Instance (VM)
resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = file(var.ssh_key_path) //function file to read content of the resource. Then, in EC2, there will be a new key with that name
}