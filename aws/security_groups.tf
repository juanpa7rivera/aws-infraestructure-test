//---- 4. Security Groups ----

// Security Group for Network Load Balancer
// - Enable access from outside to port 80
// - Enable access from LoadBalancer to Security Group of ec2 instances
resource "aws_security_group" "sg_lb" {
  name = "sg_lb_us-east_apache_001"
  description = "Network LoadBalancer - Security Group"
  vpc_id = var.vpc_id

  ingress {
    description = "http from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "TLS from VPC"
    from_port   = 22 //Port SSH
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] //from where I can access (anywhere)
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sg_lb"
    Creator = "Terraform"
  }
}

//Open firewall port that I need inside the network
//Enable SSH and HTTP
resource "aws_security_group" "allow_http_ssh" {
  name        = "allow_http_ssh"
  description = "Allow http and ssh inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    description = "http from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    security_groups = [aws_security_group.sg_lb.id]
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "TLS from VPC"
    from_port   = 22 //Port SSH
    to_port     = 22
    protocol    = "tcp"
    security_groups = [aws_security_group.sg_lb.id]
    cidr_blocks = ["0.0.0.0/0"] //from where I can access (anywhere)
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_http_ssh"
  }
}
